const mysql = require('mysql');
var env = require('./env.json')[process.env.NODE_ENV || 'local'];

const db = mysql.createPool({
    host: env.host,
    port: env.port,
    user: env.user,
    password: env.password,
    database: env.database,
    charset : 'utf8mb4'
});

module.exports.db = db;