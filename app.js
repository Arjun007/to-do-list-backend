require('dotenv').config()
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const http = require('http');
const https = require('https');
const fs = require('fs');
const cookieParser = require('cookie-parser');

var env = require('./config/env.json')[process.env.NODE_ENV || 'local'];
var app = express();
var server = '';
var port = env.listen_port;
var router = require('./routes/route');
var test_client = [];

if (
    (process.env.NODE_ENV == 'development' ||
        process.env.NODE_ENV == 'production')
) {
    var options = {
        key: fs.readFileSync(env.key),
        cert: fs.readFileSync(env.crt)
    };
    server = https.createServer(options, app);
} else {
    server = http.createServer(app);
}

// Socket operations ,if more than 5-10 socket events you are handling then create seperate file socket operation
const io = require('socket.io').listen(server);
io.set('transports', ['websocket']);

io.on('connection', (socket) => { 
    
    checkUsersBeforePush(socket);
    socket.on('disconnect', () => {
        
         removeSocket(socket);

    })
 });

 // Match new sockets user id with user id exist in array, send exit request on old socket if found, also remove id
 const checkUsersBeforePush = (_socket) => {
     console.log('socket if', _socket.id)
     test_client.forEach((item, i) => {
        console.log('item', item);
        Object.keys(item).forEach((key) => {
           console.log('key', key)
           console.log('socket id', item[key]);
           if (key === _socket.handshake.query.userId) {
               console.log('found one');
               
               io.to(item[key]).emit('imidiateexit');
               test_client.splice(i,1);
           }
        })     
     })
     test_client.push({
        [_socket.handshake.query.userId] : _socket.id
     })
     console.log('test client', test_client);
    
 }

 // On disconnect remove socket to get rid of inactive sockets
 const removeSocket = (_socket) => {

        console.log('disconnec socket id', _socket.id);
        test_client.forEach(async(item, i) => {
            Object.keys(item).forEach((key) => {
                if (item[key] === _socket.id) {
                    test_client.splice(i,1);
                }
            })     
        });
        console.log('new socket array', test_client);
   
 }




//CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header("Access-Control-ALlow-Origin", "http://localhost:4200")
    res.header('Cache-Control', 'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0');
    next();
  });
  
app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true
    })
);

app.use(cookieParser());

app.use('/',router);

server.listen(port,()=> {
    console.log('Express server listening on port %d', port);
});