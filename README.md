# To-do list Backend

**Run**

To run the project, first write `npm install` and then `npm start`. Node server will start running on **3000** port. After this, start angular project.

**Functionalities**

 - Validations with Express validator
 - User of express framework througout the project
 - Encrypt password in database using crypto
 - JWT token base login system
 - CRUD operations for view, create, update and delete in table
 - Unique login with socket.io
 - Create apis with proper promise structure