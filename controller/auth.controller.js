/** 
 * Imports 
 */

const db = require('../config/config').db;
const {validationResult} = require('express-validator');
const bcrypt = require("bcrypt");
const jwt = require('jsonwebtoken');
const env = require('../config/env.json')[process.env.NODE_ENV || 'local'];
const moment = require('moment');


/** 
 * Register User API 
 */

const registerUser = async(req,res) => {

        try {

            // Declare data
            const data = {
                ...req.params,
                input: req.body,
                output: {}
            }

            // Validate inputs
            await validateRequest(req);

            // Check if username or email exist
            await checkIfExist(data);

            // Generate Hash password
            await generateHashPassword(data);

            // Register User
            await addUser(data);

            // Send response
            res.status(200).json({
                'status': 200,
                'success': true,
                'message': 'User registered successfully',
                'data': {}
            })


        } catch (error) {

             console.log(error);
            res.status(error.status || 500).json({'status':error.status || '500','success': false,'message':error.message || 'There is something wrong'});

        }

}

 /**  
  * Validate request parameters 
  */

 const validateRequest = async(req) => {
      
    try {
        
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const err = new Error();
            err.status = 400;
            err.message = 'Invalid data provided';
            throw err; 
        }
    } catch (error) {

        throw error;

    }
  }

  /** 
   * Check if given user name or email is already exist 
   */

 const checkIfExist = async(data) => {

    try {

        return new Promise((resolve,reject) => {
            db.query('SELECT * from user where (name = "'+data.input.name+'" OR email = "'+data.input.email+'")', async (err, response) => {
                if (err) {
                    console.log('error in check sql query', err);
                    const error = new Error();
                    error.status = 400;
                    error.message = "Error in check user";
                    reject(error);
                } else { 
                    console.log('response', response)
                    if (response.length > 0) {
                        const error = new Error();
                        error.status = 400;
                        if (response[0].name === data.input.name) {
                            error.message = "Username is already exist";
                        } else if (response[0].email === data.input.email) {
                            error.message = "Email id is already exist";
                        }
                        reject(error);
                    } else {
                        resolve(data);
                    }
                }
            })
        })

    } catch(error) {
        throw error;
    }
  }

  /**  
   * Generate hash password 
   */ 

  const generateHashPassword = async(data) => {

    try {
        return new Promise((resolve, reject) => {
            bcrypt.hash(data.input.password, 10, async(err, hash) => {
                if (err) {
                    console.log('error in hash', err);
                    const error = new Error();
                    error.status = 400;
                    error.message = "Error in generating hash";
                    reject(error);
                } else {

                    data.input.password = hash;
                    resolve(data);

                }
            })
        })
    } catch (error) {
        throw error;
    }
  }

 /**  
  * Add User in database  
  */ 

 const addUser = async(data) => {

    try {
        const currentUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss');
        return new Promise((resolve, reject) => {
            db.query('INSERT INTO user (name, email, password,created_at) VALUES ("'+data.input.name+'", "'+data.input.email+'", "'+data.input.password+'", "'+currentUtc+'")', async (err, response) => {
                if (err) {
                    console.log('error in register sql query', err);
                    const error = new Error();
                    error.status = 400;
                    error.message = "Error in register user";
                    reject(error);
                } else {
                    console.log('response', response.insertId);
                    data.output.result = response;
                    resolve(data);
                }
            })
        })

    } catch (error) {

        throw error;
    }
  }


/** 
 * Login User API  
 */

const loginUser = async(req,res) => {

    try {

        // Declare data
        const data = {
            ...req.params,
            input: req.body,
            output: {}
        } 

        // Validate request
        await validateRequest(req);

        // Check if email/name exist, if yes then generate token and response
        await checkEmailAvailable(data);

        // Send Response
        res.status(200).json({
            'status': 200,
            'success': true,
            'message': 'User logged in successfully',
            'data': data.output
        })

    } catch (error) {
        console.log(error);
        res.status(error.status || 500).json({'status':error.status || '500','success': false,'message':error.message || 'There is something wrong'});
    }
    
}

/** 
 * Check in database if email/username exist, create token  
 */

const checkEmailAvailable = async(data) => {
    try {

        return new Promise((resolve, reject) => {
            db.query('SELECT * from user where (name = "'+data.input.email+'" OR email = "'+data.input.email+'")', async(err, response) => {
                if (err) {
                    console.log('error in checkemail available', err);
                    const error = new Error();
                    error.status = 400;
                    error.message = 'Error in finding user';
                    reject(error)
                } else {
                    if (response.length > 0) {
                        console.log('response', response[0]);
                        bcrypt.compare(data.input.password, response[0].password).then(async(match) => {
                            if (match) {
                                const token = jwt.sign({id: response[0].id}, env.JWT_secret, {
                                    expiresIn: "2d"
                                })
                                data.output.name = response[0].name;
                                data.output.email = response[0].email;
                                data.output.id = response[0].id;
                                data.output.token = token;
                                resolve(data);
                            } else {
                                const error = new Error();
                                error.status = 401;
                                error.message = 'Password is incorrect';
                                reject(error)
                            }
                        })
                    } else {
                        const error = new Error();
                        error.status = 400;
                        error.message = 'Incorrect Credentials';
                        reject(error)
                    }
                }
            })
        })

    } catch (error) {
        console.log('error',error);
        throw error;

    }
}

module.exports = {
    registerUser,
    loginUser
};