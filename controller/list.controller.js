/** 
 * Imports 
 */

const db = require('../config/config').db;
const {validationResult} = require('express-validator');
const env = require('../config/env.json')[process.env.NODE_ENV || 'local'];
const moment = require('moment');

/** 
 * Create new task API 
 */
const createNewTask = async(req,res) => {

        try {

            // Declare data
            const data = {
                ...req.params,
                input: req.body,
                output: {}
            }

            // Validate inputs
            await validateRequest(req);

            // Add task in db
            await addTaskInDB(data);

            // Send response
            res.status(200).json({
                'status': 200,
                'success': true,
                'message': 'Task added successfully',
                'data': {}
            })

        } catch(error) {

            console.log(error);
            res.status(error.status || 500).json({'status':error.status || '500','success': false,'message':error.message || 'There is something wrong'});

        }
}

 /**  
  * Validate request parameters 
  */

 const validateRequest = async(req) => {
      
    try {
        
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            const err = new Error();
            err.status = 400;
            err.message = 'Invalid data provided';
            throw err; 
        }
    } catch (error) {

        throw error;
    }
  }

  /** 
   * Add task in db 
   */

const addTaskInDB = async(data) => {

    try {
        const currentUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss');
        return new Promise((resolve,reject) => {
            db.query('INSERT into tasks (task_name,task_description,user_id,created_at,updated_at) VALUES ("'+data.input.taskname+'","'+data.input.description+'","'+data.id+'","'+currentUtc+'","'+currentUtc+'")', async(err, response) => {

                    if (err) {
                        console.log('error in adding task', err);
                        const error = new Error();
                        error.status = 400;
                        error.message = 'Error in adding task';
                        reject(error);
                    } else {
                        resolve(data);
                    }               
            })
        })

    } catch (error) {
        throw error;
    }
}

/** 
 * Edit task API 
 */

const editTask = async(req,res) => {

        try {

            // Declare data
            const data = {
                ...req.params,
                input: req.body,
                output: {}
            }

            // Validate request
            await validateRequest(req);

            // Update the task in database
            await updateTaskInDb(data);

            // Send response 
            res.status(200).json({
                'status': 200,
                'success': true,
                'message': 'Task Updated successfully',
                'data': {}
            })

        } catch (error) {

            console.log(error);
            res.status(error.status || 500).json({'status':error.status || '500','success': false,'message':error.message || 'There is something wrong'});
        }
}

/** 
 * Update task in DB 
 */
const updateTaskInDb = async(data) => {

        try {
            const currentUtc = moment.utc().format('YYYY-MM-DD HH:mm:ss');
            return new Promise((resolve, reject) => {
                db.query('UPDATE tasks SET task_name = "'+data.input.taskname+'", task_description = "'+data.input.description+'", updated_at = "'+currentUtc+'" WHERE id = "'+data.input.taskid+'"', async(err, response) => {

                    if (err) {
                        const error = new Error();
                        error.status = 400;
                        error.message = 'Error in updating task';
                        reject(error);
                    } else {
                        resolve(data);
                    }
                })
            })

        } catch (error) {
            throw error;
        }
}

/** 
 * Get all tasks API 
 */
const getAllTasks = async(req,res) => {

        try {

            // Declare data
            const data = {
                ...req.params,
                input: req.body,
                output: {}
            }

            // Validate request
            await validateRequest(req);

            // Get all tasks of user
            await getTaskOfUser(data);

            // Send response 
            res.status(200).json({
                'status': 200,
                'success': true,
                'message': 'Task Updated successfully',
                'data': data.output
            })

        } catch (error) {
            console.log(error);
            res.status(error.status || 500).json({'status':error.status || '500','success': false,'message':error.message || 'There is something wrong'});
        }

}

/** 
 * Get all tasks 
 */
const getTaskOfUser = async(data) => {

        try {
            return new Promise((resolve,reject) => {
                db.query('SELECT * from tasks WHERE user_id="'+data.id+'"', async(err, response) => {

                    if (err) {
                        const error = new Error();
                        error.status = 400;
                        error.message = 'Error in get tasks';
                        reject(error);
                    } else {
                        data.output = response;
                        resolve(data);
                    }
                });
            })

        } catch (error) {
            throw error;
        }
    
}

/** 
 * Delete task API 
 */
const deleteTask = async(req,res) => {

        try {

            // Declare data
            const data = {
                ...req.params,
                input: req.body,
                output: {}
            }

            // Validate request
            await validateRequest(req);

            // Delete task from database
            await deleteTaskFromDatabase(data);

            // Send response 
            res.status(200).json({
                'status': 200,
                'success': true,
                'message': 'Task deleted successfully',
                'data': {}
            })


        } catch (error) {
            console.log(error);
            res.status(error.status || 500).json({'status':error.status || '500','success': false,'message':error.message || 'There is something wrong'});
        }
}

/** 
 * Delete task from database 
 */
const deleteTaskFromDatabase = async(data) => {

        try {
            return new Promise((resolve, reject) => {
                db.query('DELETE from tasks WHERE id = "'+data.input.taskid+'"', async(err, response) => {
                    if (err) {
                        const error = new Error();
                        error.status = 400;
                        error.message = 'Error in deleting task';
                        reject(error);
                    } else {
                        resolve(data);
                    }
                })
            })

        } catch (error) {
            throw error;
        }

}

module.exports = {
    createNewTask,
    editTask,
    getAllTasks,
    deleteTask
}