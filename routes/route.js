const express = require('express');
const router = express.Router();
const expressValidation = require('../middleware/validation');
const tokenValidation = require('../middleware/verifyToken');
const authController = require('../controller/auth.controller');
const listController = require('../controller/list.controller');


/** 
 *  @method POST
 *  Register user 
 */

router.post ('/register',expressValidation.validate('register'), authController.registerUser);

/** 
 *  @method POST
 *  Login user 
 */

router.post ('/login', expressValidation.validate('login'), authController.loginUser);

/** 
 * @method POST
 * Create new task 
 */

router.post('/createtask/:id',tokenValidation, expressValidation.validate('createTask'), listController.createNewTask);

/** 
 * @method PUT
 * Edit the task 
 */

 router.put('/updatetask/:id', tokenValidation, expressValidation.validate('editTask'), listController.editTask);

 /** 
  * @method GET
  * GET all tasks of user 
  */

  router.get('/gettasks/:id', tokenValidation, expressValidation.validate('getTask'), listController.getAllTasks);

 /** 
  *  @method DELETE
  * Delete particular task 
  */

  router.delete('/deletetask/:id', tokenValidation, expressValidation.validate('deleteTask'), listController.deleteTask);



module.exports = router;