const jwt = require('jsonwebtoken');
const env = require('../config/env.json')[process.env.NODE_ENV || 'local'];

module.exports = (req,res,next) => {

    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, env.JWT_secret);
        const userId = decodedToken.id;
        if (req.params.id && req.params.id == userId) {
            next();
        } else {
            res.status(401).json({
                'status': 401,
                'success': false,
                'message': 'Unauthorized User',
                'data': {}
            })
           
        }
    } catch (error) {
        res.status(401).json({
            'status': 401,
            'success': false,
            'message': 'Unauthorized User',
            'data': {}
        })
    }

}