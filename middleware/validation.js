const { body, param } = require('express-validator');

exports.validate = (method) => {
    switch(method) {
        case 'register' : {
            return [
                body('name', 'User name is required').exists(),
                body('email', 'Email is required').exists(),
                body('password', 'Password is required').exists()
            ]
        }
        
        case 'login' : {
            return [
                body('email', 'Please enter Email Id').exists(),
                body('password', 'Please enter password').exists()
            ]
        }

        case 'createTask' : {
            return [
                body('taskname', 'Please provide task name').exists(),
                body('description', 'Please provide task description').exists()
            ]
        }

        case 'editTask' : {
            return [
                body('taskid', 'Please provide task id').exists(),
                body('taskname', 'Please provide task name').exists(),
                body('description', 'Please provide task description').exists()
            ]
        }

        case 'getTask' : {
            return [
                param('id', 'Please provide user id').exists()
            ]
        }

        case 'deleteTask' : {
            return [
                body('taskid', 'Please provide task id').exists()
            ]
        }
    }
}